from odoo import models, fields, api
from odoo.exceptions import ValidationError
import pprint

pp = pprint.PrettyPrinter(depth=5)


class SaleOrderInherit(models.Model):

    _inherit = 'sale.order'
    name = fields.Char(string="Nuevo campo")

    @api.depends('order_line.price_total')
    def _amount_all(self):
        print("AMOUNTT - -- ->", self)
        print("AMOUNTT type- -- ->", type(self))
        # method_list = [func for func in dir(self)
        #                if callable(getattr(self, func))]
        # print(*method_list, sep="\n")
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
                amount_total = amount_untaxed + amount_tax
                print("AMOUNT TOTAL", amount_total)
            order.update({
                'amount_untaxed': amount_untaxed,
                'amount_tax': amount_tax,
                'amount_total': amount_untaxed + amount_tax,
            })

    @api.model
    def create(self, vals):
        print("DENTRO DEL CREATE - -- ->")
        # pp.pprint(vals)
        price_unit_1 = 0
        for item in vals['order_line']:
            print(item[2]['price_unit'])
            price_unit_1 += item[2]['price_unit']
            print(price_unit_1)

        if price_unit_1 < 50000:
            raise ValidationError("Tenés que gastar más de 50000.")

        result = super(SaleOrderInherit, self).create(vals)
        return result

    def write(self, vals):
        "ESTOY EN EL WRITE"
        res = super(SaleOrderInherit, self).write(vals)
        raise ValidationError("No se puede guardar.")

        return res
