{
    'name': "sale_order_1",
    'summary': "Practica sale order",
    'version': '1.0',
    'depends': ['base', 'mail', 'sale'],
    'author': "Author Name",
    'category': 'Category',
    'description': """
    Description text
    """,
    # data files always loaded at installation
    'data': [
        'views/sale_order_inherit.xml',

    ],

}
