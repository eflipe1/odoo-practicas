# -*- coding: utf-8 -*-
# link: https://youtu.be/-3_A1oT71DY
from odoo import models, fields, api

'''
Setting -> Tenico -> Data structure -> buscamos el queremos modificar
-> stock.move.lots -> vemos los campos

Vamos a crear dos campos nuevos.
'''


class ResPartner(models.Model):
    _inherit = "res.partner"
    _description = "Herencia modulo res.partner"

    name = fields.Char('Nombre de de la Industry')
    lista_stock = fields.Many2one(
        string="Lista de stock",
        comodel_name="res.partner.industry",
        help="Explain your field.",
        index=True
    )

# class modulo_video(models.Model):
#     _name = 'modulo_video.modulo_video'
#     _description = 'modulo_video.modulo_video'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
