# odoo-practicas
Link: https://www.odoo.com/documentation/12.0/es/developer/howtos/backend.html

# Views
Module data is declared via data files, XML files with <record> elements. Each <record> element creates or updates a database record.
Views define the way the records of a model are displayed.

Ejemplo:

<record model="ir.actions.act_window" id="action_list_ideas">
    <field name="name">Ideas</field>
    <field name="res_model">idea.idea</field>
    <field name="view_mode">tree,form</field>
</record>
<menuitem id="menu_ideas" parent="menu_root" name="Ideas" sequence="10"
          action="action_list_ideas"/>

Generic view declaration
A view is declared as a record of the model ir.ui.view. The view type is implied by the root element of the arch field:

<record model="ir.ui.view" id="view_id">
    <field name="name">view.name</field>
    <field name="model">object_name</field>
    <field name="priority" eval="16"/>
    <field name="arch" type="xml">
        <!-- view content: <form>, <tree>, <graph>, ... -->
    </field>
</record>
