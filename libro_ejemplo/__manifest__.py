{
    'name': 'chapter_03_libro_ejemplo',
    'summary': "Ejemplo del libro",
    'description': """
Algina descripción copada.
==============
Description related to the module.
""",
    'author': "Your name",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '13.0.1',
    'depends': ['base', 'sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/library_module_views.xml',
        'views/sale_inherit.xml',
        # 'views/sale_order_line_inherit.xml'
    ],

}
