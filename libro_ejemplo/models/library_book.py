from odoo import models, fields, api


class LibraryModule(models.Model):
    _name = "library.module"
    _order = 'date_release desc, name'
    name = fields.Char("Title", required=True)
    date_release = fields.Date(string="Release Date", )
    author_ids = fields.Many2many(
        string="Authors",
        comodel_name="res.partner",
    )
    cover = fields.Binary('Book Cover')

    publisher_id = fields.Many2one(
        'res.partner', string='Publisher',
        # optional:
        ondelete='set null',
        context={},
        domain=[],
    )
    publisher_city = fields.Char(
        'Publisher City',
        related='publisher_id.city',
        readonly=True)

    @api.model
    def _referencable_models(self):
        models = self.env['ir.model'].search([
            ('field_id.name', '=', 'message_ids')
        ])
        return [(x.model, x.name) for x in models]

    ref_doc_id = fields.Reference(
        selection='_referencable_models',
        string='Reference Document'
    )


class ResPartner(models.Model):
    _inherit = 'res.partner'
    published_book_ids = fields.One2many(
        'library.module', 'publisher_id',
        string='Published Books')

    authored_book_ids = fields.Many2many(
        'library.module',
        string='Authored Books',

    )
