from odoo import models, fields, api
import pprint

pp = pprint.PrettyPrinter(depth=5)


class SaleInherit(models.Model):
    _inherit = "sale.order"
    mensaje = fields.Char(string='Messageeeee')
    mensaje_2 = fields.Char(string='Message número 2',
                            compute='random_mensaje')

    print("inherit ----------------->", _inherit)
    print("inherit ----------------->", type(_inherit))

    def show_info(self):
        print("HOlaaaaa", self)
        # method_list = [func for func in dir(self)
        #                if callable(getattr(self, func))]
        # print(*method_list, sep="\n")
        env_model = self.env['sale.order.line']
        search_info = env_model.search([])
        print("INFO ------->>>", type(search_info))
        for item in search_info:
            print(item.price_unit)
        #print("INFO ------->>>", dir(search_info))
        #print("INFO ------->>>", search_info.price_unit)
        return True

    def random_mensaje(self):
        # print('price_unit', self.price_unit)  # muestra el nombre del modelo y el ID
        # print('amount_total: ID', self.price_unit.id)
        # # print('driver_id: NAME', self.driver_id.name)
        self.mensaje_2 = f'Hola {self.user_id.name}'

    def random_form(self):
        # print("PARTNER --------->", self.partner_id)
        # print("PARTNER --------->", type(self.partner_id))
        # # print("PARTNER --------->", dir(self.partner_id))
        # method_list = [func for func in dir(self.partner_id)
        #                if callable(getattr(self.partner_id, func))]
        # print("METODOS", method_list)
        # fields = [f for f in dir(self.partner_id) if not callable(
        #     getattr(self.partner_id, f)) and not f.startswith('__')]
        # print("FIELDS", fields)
        self.mensaje = f'PARTNER ID: {self.partner_id}'

    @ api.model  # en odoo13 se agrega por default
    def create(self, vals):
        print("Estoy dentro del create de SALE ORDER")
        # pp.pprint(vals)
        # print("Vals dict", vals)
        print("PRICE SALE ORDER---->", vals['order_line'][0][2]['price_unit'])
        # print("PRICE TYPE ---->", type(vals['price_unit']))
        # vals['price_unit'] = vals['price_unit'] * 1.2
        # print("NUEVO PRICE ---->", vals['price_unit'])
        result = super(SaleInherit, self).create(vals)
        return result

    # total_sales = fields.Many2many(
    #     'sale.order', string='Total')
    # sum_total = fields.Integer('Total',
    #                            compute='_compute_count_books')
    #
    # @api.depends('authored_book_ids')
    # def _compute_count_books(self):
    #     for r in self:
    #         r.sum_total = len(r.total_sales)
