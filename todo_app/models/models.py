# -*- coding: utf-8 -*-

from odoo import models, fields, api
import pprint
pp = pprint.PrettyPrinter(depth=5)


class TodoTask(models.Model):
    _name = "todo.task"
    _description = "To-do app task"
    name = fields.Char("Description", required=True)
    is_done = fields.Boolean('Done?')
    active = fields.Boolean(string='Active?', default=True)

    @api.multi
    def do_toggle_done(self):
        print("SELF DEL TOGGLE", self)

        for task in self:

            print("TASK: ", task)
            task.is_done = not task.is_done
        return True

    @api.multi
    def do_clear_done(self):
        print("SELF DEL CLEAR DONE", self)
        fields = [f for f in dir(self) if not callable(
            getattr(self, f)) and not f.startswith('__')]
        pp.pprint(fields)
        dones = self.search([('is_done', '=', True)])
        dones.write({'active': False})
        return True
