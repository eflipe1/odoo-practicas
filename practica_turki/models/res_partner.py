from odoo import models, fields


class ResPartner(models.Model):
    _inherit = "res.partner"
    mensaje = fields.Char(string='Message')
    mensaje_2 = fields.Char(string='Message número 2')
    car_count = fields.Integer(string="Count", compute="get_car_number")

    def get_car_number(self):
        for record in self:
            record.car_count = self.env['fleet.vehicle'].search_count([('driver_id', '=', self.id)])

    def get_cars(self):
        # relacion entre res.partner con fleet.vehicle, para cuando haces click en el nuevo boton
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Cars',
            'view_mode': 'tree',
            'res_model': 'fleet.vehicle',
            'domain': [('driver_id', '=', self.id)],
            'context': "{'create':False}"
        }
