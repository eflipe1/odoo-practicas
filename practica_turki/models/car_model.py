from odoo import models, fields, api
from odoo.exceptions import ValidationError


class CarModel(models.Model):
    _name = "car.model"
    _description = "car module de turki"
    name = fields.Char(string="Nombre del auto")
    horse_power = fields.Integer(
        string="Caballos de fuerza",
    )
    door_number = fields.Integer(
        string="Door numbers",
    )

    total_speed = fields.Integer(
        string="Total speed", compute="get_total_speed")
    random_msg = fields.Char(string="Mensaje", compute='random_mensaje')

    driver_id = fields.Many2one(
        string="Drivers",
        comodel_name="res.partner",
    )
    parking_id = fields.Many2one(
        string="Parking",
        comodel_name="parking.model",
    )

    feature_ids = fields.Many2many(
        'feature.model',
        string="Features name",
    )

    status = fields.Selection(
        string="Status",
        selection=[
            ('new', 'New'),
            ('used', 'Used'),
            ('sold', 'Sold'),
        ],
        default="new"
    )

    # Create
    @api.model  # en odoo13 se agrega por default
    def create(self, vals):
        print("Estoy dentro del create")
        print("Vals dict", vals)
        print("Name", vals['name'])
        if vals['name'] == 'abcd':
            vals['name'] = "Pepe"
        result = super(CarModel, self).create(vals)
        return result

    # Write: para edit
    def write(self, vals):
        print("Dentro del write")
        if vals['horse_power'] == 4:
            vals['horse_power'] = 10
            raise ValidationError("Mensajito de advertencia.")
        result = super(CarModel, self).write(vals)
        return result

    # Delete
    def unlink(self):
        print("Borrando")
        # exception singleton (tiene > de un record)
        for rec in self:
            if rec.horse_power == 9:
                raise ValidationError("Mensaje del delete")

        return super(CarModel, self).unlink()

    def set_car_to_used(self):
        self.status == 'used'

    def set_car_to_sold(self):
        self.status == 'sold'

    def get_total_speed(self):
        print("name", self.name)
        self.total_speed = self.horse_power * 100

    def random_mensaje(self):
        # muestra el nombre del modelo y el ID
        print('driver_id', self.driver_id)
        print('driver_id: ID', self.driver_id.id)
        print('driver_id: NAME', self.driver_id.name)
        self.random_msg = f'Hola {self.driver_id.name}'


class ParkingModel(models.Model):
    _name = "parking.model"
    _description = "modulo parking"

    name = fields.Char()
    car_ids = fields.One2many(
        "car.model",
        "parking_id",
        string="Cars",
    )


class FeatureModel(models.Model):
    _name = "feature.model"
    _description = "Feature model"

    name = fields.Char(string="Name")
    value = fields.Char(string="Features")
